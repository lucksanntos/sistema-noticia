@extends('layouts.admin')
@section('titulo','Área Administrativa')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Visualizar Notícia</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <table class="table table-striped table-condensed">
                    <tr>
                        <th width="150">ID</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th width="150">Título</th>
                        <td>Exemplo 157</td>
                    </tr>
                    <tr>
                        <th width="150">Subtítulo</th>
                        <td>Artigo</td>
                    </tr>
                    <tr>
                        <th width="150">Descrição</th>
                        <td>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla incidunt delectus odit reiciendis. Animi eos praesentium ut laudantium harum qui, tempore provident vero perspiciatis, culpa doloremque! Dolor velit est cupiditate. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro harum itaque tempora quos? Veniam nostrum accusamus voluptatibus consectetur magni. Dolorem unde repellendus voluptatem ea, blanditiis suscipit sequi veniam perspiciatis laboriosam.</td>
                    </tr>
                    <tr>
                        <th width="150">Status</th>
                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates fugiat reprehenderit animi asperiores nisi sed maxime, rerum maiores tempora, ea ipsam nobis nesciunt assumenda dolore porro voluptatem aliquid mollitia consequuntur.</td>
                    </tr>
                </table>
                <a href="#" class="btn btn-danger">Editar Notícia</a>
                <a href="#" class="btn btn-secondary">Cancelar</a>
            </div>
        </div>

    </div>



@endsection