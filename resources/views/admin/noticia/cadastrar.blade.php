@extends('layouts.admin')
@section('titulo','Área Administrativa')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Cadastrar Notícia</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                @include('admin.noticia.form')
            </div>
        </div>

    </div>



@endsection