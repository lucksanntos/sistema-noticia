@extends('layouts.admin')
@section('titulo','Área Categorias')
    
@section('conteudo')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Categorias
                
             </h2>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>status</th>
                        <th>Quantidade Notícias</th>
                        <th>Data de Cadastro</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row">1</td>
                        <td>Ação</td>
                        <td>Ativo</td>
                        <td>3</td>
                        <td>21/05/2019</td>
                        <td>
                            <a href="#" class="btn btn-sm btn-segundary">
                                <i class="fas fa-eye">
                                    
                                </i>
                            </a>
                            <a href="#" class="btn btn-sm btn-warning">
                                <i class="fas fa-edit">
                                    
                                </i>
                            </a>
                            <a href="#" class="btn btn-sm btn-danger">
                                <i class="fas fa-trash">
                                    
                                </i>
                            </a>
                        </td>
                        
                    </tr>
                   
                </tbody>
            </table>
        </div>
    </div>
    
</div>
    
@endsection